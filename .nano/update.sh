#!/bin/sh

WORKDIR=$(pwd)
BASEDIR=$(dirname "$0")
NANORC_FILE="~/.nanorc"

# check for unzip before we continue
if [ ! "$(command -v unzip)" ]; then
  echo 'unzip is required but was not found. Install unzip first and then run this script again.' >&2
  exit 1
fi

_fetch_sources() {
  wget -O /tmp/nanorc.zip https://github.com/scopatz/nanorc/archive/master.zip
  mkdir -p "$BASEDIR/scopatz"
  cd "$BASEDIR/scopatz" || exit
  unzip -o "/tmp/nanorc.zip"
  mv nanorc-master/* ./
  rm -rf nanorc-master
  rm /tmp/nanorc.zip
  cd "$WORKDIR"
}

_update_nanorc() {
  cd "$BASEDIR"
  touch nanorc

  for file in *.nanorc; do
    inc="include \"$file\""
    if ! grep -Fxq "$inc" nanorc ; then
      echo "$inc" >> nanorc
      echo "[INCLUDE] $file"
    fi
  done

  echo "--- DONE ---"

  cd "$WORKDIR"
}

_update_nanorc_lite() {
  sed -i '/include "\/usr\/share\/nano\/\*\.nanorc"/i include "~\/.nano\/*.nanorc"' "${NANORC}"
}

case "$1" in
 -l|--lite)
   UPDATE_LITE=1;;
 -f|--fetch-souces)
   FETCH_SOURCES=1;;
 -h|--help)
   echo "Install script for nanorc syntax highlights"
   echo "Call with -l or --lite to update .nanorc with secondary precedence to existing .nanorc includes"
   echo "Call with -f or --fetch-sources to update scopatz' resources"
   exit 0
 ;;
esac

if [ $FETCH_SOURCES ]; then
  _fetch_sources;
elif [ $UPDATE_LITE ]; then
  _update_nanorc_lite
else
  _update_nanorc
fi

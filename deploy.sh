#!/bin/sh

SCRIPT_DIR="$( cd "$( dirname "$BASH_SOURCE[0]" )" && pwd )"

DEFAULT_MANIFEST="MANIFEST"

if [ -z "$1" ]; then
    manifest_file="$DEFAULT_MANIFEST"
else
    manifest_file="$1"
fi

resolveDestination() {
    local destination="$1"

    if [ -n "$destination" ]; then
        if [ "${destination#~}" != "$destination" ]; then
            destination="${HOME}${destination#~}"
        fi

        if [ "${destination#/}" = "$destination" ]; then
            destination="${HOME}/$destination"
        fi
    fi

    echo "$destination"
}

linkFile() {
    local filename="$SCRIPT_DIR/$1"
    local destination
    local link_type

    if [ -n "$3" ]; then
        destination="$2"
        link_type="$3"
    elif [ "$2" = "soft" ] || [ "$2" = "hard" ]; then
        destination="$HOME/$1"
        link_type="$2"
    else
        destination="$2"
        link_type="soft"
    fi

    destination=$(resolveDestination "$destination")

    if [ -z "$destination" ]; then
        destination="$HOME/$1"
    fi

    mkdir -p $(dirname "$destination")

    if [ -L "$destination" ]; then
        local rp=$(realpath $destination)

        if [ "$rp" = "$filename" ]; then
            echo "[SKIPPING] $destination -> $filename already symlinked"
        else
            echo "[ERROR] symlink exists but its target is invalid: $destination -> $rp Please fix manually." && exit 1
        fi

    elif [ -f "$destination" -a -f "$filename" ]; then
        echo "[WARNING] could not link: file already exists: $destination"

    elif [ -d "$destination" -a -d "$filename" ]; then
        echo "[WARNING] could not link: directory already exists: $destination"

    elif [ -e "$destination" ]; then
        echo "[ERROR] $destination exists but it's neither a symlink nor of the correct type. Please fix manually." && exit 1

    else
        if [ "$link_type" = "hard" ]; then
            ln "$filename" "$destination"
        else
            ln -s "$filename" "$destination"
        fi
        echo "[OK] $destination -> $filename ($link_type)"
    fi
}

copyFile() {
    local filename="$SCRIPT_DIR/$1"
    local destination
    local force="$3"

    if [ -n "$2" ]; then
        destination="$2"
    else
        destination="$HOME/$1"
    fi

    destination=$(resolveDestination "$destination")

    if [ -z "$destination" ]; then
        destination="$HOME/$1"
    fi

    mkdir -p $(dirname "$destination")

    if [ -f "$destination" ] && [ "$force" != "force" ]; then
        echo "[WARNING] could not copy: file already exists: $destination"
    else
        cp "$filename" "$destination"
        echo "[OK] copied $filename to $destination"
    fi
}

deployManifest() {
    for row in $(cat $SCRIPT_DIR/$1); do
        local operation=$(echo $row | cut -d \| -f 1)
        local filename=$(echo $row | cut -d \| -f 2)
        local destination=$(echo $row | cut -d \| -f 3)

        case $operation in
            symlink|hardlink)
                local link_type="soft"
                if [ "$operation" = "hardlink" ]; then
                    link_type="hard"
                fi
                
                case $filename in
                    */\*)
                        folder="${filename%/*}"
                        for file in "$folder"/*; do
                            if [ -f "$file" ]; then
                                filebase=$(basename "$file")
                                linkFile "$file" "$destination/$filebase" "$link_type"
                            fi
                        done
                        ;;
                    *)
                        linkFile "$filename" "$destination" "$link_type"
                        ;;
                esac
                ;;
            copy)
                copyFile "$filename" "$destination"
                ;;
            fcopy)
                copyFile "$filename" "$destination" "force"
                ;;
            *)
                echo "[WARNING] Unknown operation $operation. Skipping..."
                ;;
        esac
    done
}

updateBashRC() {
    if ! grep -q "~/.bashc" "$HOME/.bashrc"; then
        printf "\n# source bash commons from dotfiles repository\nif [ -f ~/.bashc ]; then\n    . ~/.bashc\nfi\n" >> "$HOME/.bashrc"
        echo "[OK] added ~/.bashc execution"
    else
        echo "[WARNING] source ~/.bashc might already exist"
    fi
}

echo "--- Common configs ---"
deployManifest "$manifest_file"


echo "--- Update .bashrc ---"
updateBashRC

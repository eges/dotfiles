alias eric='echo "Hallu :))"'

# Archives
alias targz='tar -xvzf'
alias tarbz2='tar -xvjf'

# Docker Compose
alias dcd='docker compose down'
alias dcu='docker compose up -d'
alias dcua='docker compose up'
alias ndc='nano docker-compose.yml'

# bashrc
alias sbrc='source ~/.bashrc'

# Laravel
alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'

# common
alias la='ls -la'
alias ll='ls -l'

mkdir -p $HOME/.trash

del() {
    local destdir=$(mktemp -d $HOME/.trash/trash-XXXXXXXXX);
    mkdir $destdir/files
    echo $0 $@ >$destdir/command
    set >$destdir/environ
    mv -t $destdir/files $@
} 

rm() {
   date >>$HOME/.rm.log +"[%a %d %b %Y %T] rm $@"
   /bin/rm $@
}
